import 'package:flutter/material.dart';

class Antwort extends StatelessWidget {
  final Function setHandler;
  final String antwortTextOutput;

  Antwort(this.setHandler, this.antwortTextOutput);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text(antwortTextOutput),
        textColor: Colors.white,
        color: Colors.lightGreen[500],
        onPressed: setHandler,
      ),
    );
  }
}
