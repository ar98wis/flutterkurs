import 'package:flutter/material.dart';

import './frage.dart';

class Frage extends StatelessWidget {
  final String fragenText;

  Frage(this.fragenText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(20),
      child: Text(
        fragenText,
        style: TextStyle(fontSize: 28),
        textAlign: TextAlign.center,
      ),
    );
  }
}
