import 'package:flutter/material.dart';

import 'antwort.dart';
import 'frage.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> fragen;
  final int fragenIndex;
  final Function antworteFrage;

  Quiz(
      {@required this.fragen,
      @required this.fragenIndex,
      @required this.antworteFrage});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Frage(
          fragen[fragenIndex]['fragenText'],
        ),
        ...(fragen[fragenIndex]['antworten'] as List<Map<String, Object>>).map(
          //anonyme Methode
          (antwort) {
            return Antwort(
                () => antworteFrage(antwort['score']), antwort['text']);
          },
        ).toList()
      ],
    );
  }
}
