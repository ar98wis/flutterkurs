import 'package:flutter/material.dart';
import 'package:project1/ergebnis.dart';

/* import 'package:project1/frage.dart';
import './antwort.dart'; */
import './quiz.dart';
import './ergebnis.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _fragen = const [
    //Die geschwungenen Klammer.equals(Map()) = true)
    {
      'fragenText': 'Wähle eine der Folgenden',
      'antworten': [
        {'text': 'Rot', 'score': 5},
        {'text': 'Blau', 'score': 6},
        {'text': 'Grün', 'score': 7},
        {'text': 'Gelb', 'score': 4}
      ],
    },
    {
      'fragenText': 'Was ist dein Lieblingstier',
      'antworten': [
        {'text': 'Löwe', 'score': 10},
        {'text': 'Elefant', 'score': 5},
        {'text': 'Schlange', 'score': 1},
        {'text': 'Adler', 'score': 2}
      ],
    },
    {
      'fragenText': 'Wie alt bist du?',
      'antworten': [
        {'text': 'jünger als 18', 'score': 5},
        {'text': '18-29', 'score': 5},
        {'text': '29-39', 'score': 5},
        {'text': '39-100', 'score': 5}
      ],
    },
  ];

  var _fragenIndex = 0;
  var _totalScore = 0;
  void _antworteFrage(int score) {
    _totalScore += score;
    setState(() {
      _fragenIndex++;
    });
    print("Frage beantwortet");
    print(_fragenIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Meine Fragebogen-App"),
        ),
        body: _fragenIndex < _fragen.length
            ? Quiz(
                fragen: _fragen,
                fragenIndex: _fragenIndex,
                antworteFrage: _antworteFrage,
              )
            /*RaisedButton(
              child: Text("Antwort 1"),
              onPressed: _antworteFrage, //ruft die Methode antworteFrage() auf.
              //würde hier 'antworteFrage()' stehen, würde der Rückgabewert der Methode zurückgegeben werden
            ),
            RaisedButton(
              child: Text("Antwort 2"),
              onPressed: () => print(
                  "Frage mit 2 beantwortet"), //anonyme Methode ohne Parameter, erfüllt gleiche Aufage;tldr anonyme Methoden nutzen für einmalig benötigte Funktionen
            ),
            RaisedButton(
                child: Text("Antwort 3"),
                onPressed: () {
                  //anonyme Methode mit Methodenkörper
                  //...
                  print("Frage mit 3 beantwortet");
                })*/

            : Ergebnis(_totalScore),
      ),
    );
  }
}
