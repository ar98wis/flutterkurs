import 'package:flutter/material.dart';

class Ergebnis extends StatelessWidget {
  final int ergebnisScore;

  Ergebnis(this.ergebnisScore);

  String get ergebnisAusgasbe {
    String ergebnisText;

    if (ergebnisScore <= 12) {
      ergebnisText = "Nicht schlecht..";
    } else if (ergebnisScore <= 15) {
      ergebnisText = "Sauber";
    } else {
      ergebnisText = "1A Persönlichkeit";
    }
    return ergebnisText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        ergebnisAusgasbe,
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
